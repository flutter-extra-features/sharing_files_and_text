import 'package:flutter/material.dart';
import 'package:share_files_app/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}

/* 
  Packages Used :
    1] flutter pub add image_picker   ==> used for extracting images from gallery
    2] flutter pub add share_plus   ==> used specific version 8.0.2 because in new version shareWithResult() method is not defined
*/